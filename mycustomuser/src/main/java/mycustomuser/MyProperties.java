package mycustomuser;

import java.util.Enumeration;
import java.util.ResourceBundle;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.language.UTF8Control;
import com.liferay.portal.kernel.model.Resource;
import com.liferay.portal.kernel.service.ServiceWrapper;

@Component(
		property = {
				"language.id=en_US"
		},
		service = ResourceBundle.class
	)
public class MyProperties extends ResourceBundle {
	
	private final ResourceBundle resource = ResourceBundle.getBundle("content.Language", UTF8Control.INSTANCE);

	@Override
	public Enumeration<String> getKeys() {
		return resource.getKeys();
	}

	@Override
	protected Object handleGetObject(String arg0) {
		return resource.getObject(arg0);
	}
	
	

	
}

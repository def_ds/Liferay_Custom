/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pl.job.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class JobSoap implements Serializable {
	public static JobSoap toSoapModel(Job model) {
		JobSoap soapModel = new JobSoap();

		soapModel.setJobId(model.getJobId());
		soapModel.setName(model.getName());
		soapModel.setType(model.getType());

		return soapModel;
	}

	public static JobSoap[] toSoapModels(Job[] models) {
		JobSoap[] soapModels = new JobSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static JobSoap[][] toSoapModels(Job[][] models) {
		JobSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new JobSoap[models.length][models[0].length];
		}
		else {
			soapModels = new JobSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static JobSoap[] toSoapModels(List<Job> models) {
		List<JobSoap> soapModels = new ArrayList<JobSoap>(models.size());

		for (Job model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new JobSoap[soapModels.size()]);
	}

	public JobSoap() {
	}

	public int getPrimaryKey() {
		return _jobId;
	}

	public void setPrimaryKey(int pk) {
		setJobId(pk);
	}

	public int getJobId() {
		return _jobId;
	}

	public void setJobId(int jobId) {
		_jobId = jobId;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		_type = type;
	}

	private int _jobId;
	private String _name;
	private String _type;
}